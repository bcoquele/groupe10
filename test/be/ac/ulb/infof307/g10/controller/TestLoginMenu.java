package be.ac.ulb.infof307.g10.controller;

import be.ac.ulb.infof307.g10.util.DuplicateKeyException;
import be.ac.ulb.infof307.g10.model.DAO.DatabaseDAO;
import be.ac.ulb.infof307.g10.model.Hasher;
import de.saxsys.javafx.test.JfxRunner;
import de.saxsys.javafx.test.TestInJfxThread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import static junit.framework.TestCase.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * In order for some tests to work, the javafx dialog are supposed to be commented in the tested methods
 * or else the util java.lang.IllegalStateException: Toolkit not initialized is gonna be thrown.
 */
// The following line is supposed to solve the problem with the javafx dialogs. It doesn't.
// It works only with JUnit4 I think.
@RunWith(JfxRunner.class)
class TestLoginMenu {

    private LoginMenu loginMenu;
    private DatabaseDAO databaseDAO;
    private Hasher hashing;

    @BeforeEach
    void init() throws NoSuchAlgorithmException {
        MainApp mainApp = new MainApp();
        loginMenu = new LoginMenu(mainApp);
        databaseDAO = DatabaseDAO.getInstance();
        try {
            loginMenu.register("test","register");
        } catch (SQLException e) {}
    }

    /**
     * Test if the method register() adds correctly the user to the databaseDAO by checking if it's present.
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     */
    @Test
    // The following line is supposed to solve the problem with the javafx dialogs. It doesn't.
    // It works only with JUnit4 I think.
    @TestInJfxThread
    void registerOneUser() throws SQLException, NoSuchAlgorithmException {
        databaseDAO.open();
        assertTrue(databaseDAO.checkUser("test", Hasher.hashPassword("register")));
        databaseDAO.close();
    }

    /**
     * Test if the duplicate key util is thrown.
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     */
    @Test
    // The following line is supposed to solve the problem with the javafx dialogs. It doesn't.
    // It works only with JUnit4 I think.
    @TestInJfxThread
    void userAlreadyExist() throws NoSuchAlgorithmException, SQLException {
        try{
            loginMenu.register("test","register");
            fail("The test didn't detect the duplicate key.");
        } catch (DuplicateKeyException e) {
            assertTrue(true);
        }
    }


}