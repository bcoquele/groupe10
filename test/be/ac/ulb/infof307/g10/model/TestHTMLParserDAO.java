package be.ac.ulb.infof307.g10.model;

import be.ac.ulb.infof307.g10.model.DAO.HTMLParserDAO;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.*;

class TestHTMLParserDAO {

    private HTMLParserDAO parser = new HTMLParserDAO();

    @Test
    void getXmlLinkListLemonde() {
        parser = new HTMLParserDAO();
        String url = "https://www.lemonde.fr";
        try {
            parser.rssListFinder(url);
        } catch (IOException e) {
            e.printStackTrace();
            assertNotEquals(false, false);
        }
        List<String> xmlList = parser.getXmlLinkList();
        System.out.println(xmlList);
        assertNotEquals(0,xmlList.size());
    }

    @Test
    void getXmlLinkListLesoir() {
        parser = new HTMLParserDAO();
        String url = "https://lesoir.be/";
        try {
            parser.rssListFinder(url);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Couldn't parse website: "+url);
        }
        List<String> xmlList = parser.getXmlLinkList();
        System.out.println(xmlList);
        assertNotEquals(0,xmlList.size());
    }

    @Test
    void getXmlLinkListLaDh() {
        parser = new HTMLParserDAO();
        String url = "https://dhnet.be/";
        try {
            parser.rssListFinder(url);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Couldn't parse website: "+url);
        }
        List<String> xmlList = parser.getXmlLinkList();
        System.out.println(xmlList);
        assertNotEquals(0,xmlList.size());
    }

    @Test
    void getXmlLinkListNYTimes() {
        parser = new HTMLParserDAO();
        String url = "https://nytimes.com";
        try {
            parser.rssListFinder(url);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Couldn't parse website: "+url);
        }
        List<String> xmlList = parser.getXmlLinkList();
        System.out.println(xmlList);
        assertNotEquals(0, xmlList.size());
    }

    @Test
    void forceUnknownHostException() {
        parser = new HTMLParserDAO();
        String wrong_url = "https://mqlsdkqsmldmlkqlmdqks.com";
        try {
            parser.rssListFinder(wrong_url);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            assertTrue(true);
        } catch (IOException e) {
            e.printStackTrace();
            fail("Wrong excpetion type is thrown");
        }
    }

    @Test
    void contentParsing(){
        try {
            String content= HTMLParserDAO.contentParsing("https://www.lesoir.be/212247/article/2019-03-14/le-cardinal-godfried-danneels-est-decede");
            System.out.println(content);
            assertNotEquals(null, content);

            content= HTMLParserDAO.contentParsing("https://www.dhnet.be/sports/football/europe/championnatsetrangers/zinedine-zidane-tient-son-premier-transfert-au-real-madrid-5c8a369fd8ad5878f002f670");
            System.out.println(content);
            assertNotEquals(null, content);

            content=HTMLParserDAO.contentParsing("https://www.lemonde.fr/planete/article/2019/03/14/naufrage-du-grande-america-la-nappe-de-petrole-pourrait-toucher-le-littoral-francais-dimanche-ou-lundi_5435823_3244.html");
            System.out.println(content);
            assertNotEquals(null, content);

        } catch (IOException e) {
            fail("Couldn't parse content.");
        }

    }

}