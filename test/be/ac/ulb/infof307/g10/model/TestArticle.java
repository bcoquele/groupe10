package be.ac.ulb.infof307.g10.model;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Can't run the tests because of an attribut that uses JavaFX
 */
class TestArticle {

    @Test
    void getTitle() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        assertEquals("Meurtre",article.getTitle());
    }

    @Test
    void getContent() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        assertEquals("Blabla", article.getContent());
    }

    @Test
    void getAuthor() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        assertEquals("Jean",article.getAuthor());
    }

    @Test
    void setTitle() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        article.setTitle("Bataille");
        assertEquals("Bataille", article.getTitle());
    }

    @Test
    void setAuthor() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        article.setAuthor("Mathieu");
        assertEquals("Mathieu",article.getAuthor());
    }

    @Test
    void setContent() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        article.setContent("Gnahgnahgnah");
        assertEquals("Gnahgnahgnah",article.getContent());
    }

    @Test
    void getSource() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        assertEquals("Le Soir", article.getSource());
    }

    @Test
    void getThumbnail() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        assertEquals("image",article.getThumbnail());
    }

    @Test
    void getLocalisation() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        article.setLocalisation("France");
        assertEquals("France", article.getLocalisation());
    }

    @Test
    void getKeyWords() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        ArrayList<String> keywords = new ArrayList<>();
        keywords.add("violence");
        keywords.add("tuerie");
        article.setKeyWords(keywords);
        assertEquals(keywords,article.getKeyWords());
    }

    @Test
    void getDate() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        String date ="1er Janvier 2000";
        article.setDate(date);
        assertEquals(date, article.getDate());
    }

    @Test
    void setSource() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        article.setSource("La DH");
        assertEquals("La DH", article.getSource());
    }

    @Test
    void setThumbnail() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        article.setThumbnail("thumbnail");
        assertEquals("thumbnail", article.getThumbnail());
    }

    @Test
    void setLocalisation() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        article.setLocalisation("France");
        assertEquals("France", article.getLocalisation());
    }

    @Test
    void setKeyWords() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        ArrayList<String> keywords = new ArrayList<>();
        keywords.add("violence");
        keywords.add("tuerie");
        article.setKeyWords(keywords);
        assertEquals(keywords,article.getKeyWords());
    }

    @Test
    void setDate() {
        Article article = new Article("Meurtre", "Jean","Le Soir","image");
        String date = "1er Janvier 2000";
        article.setDate(date);
        assertEquals(date, article.getDate());
    }
}