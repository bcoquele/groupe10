package be.ac.ulb.infof307.g10.model;

import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestHasher {

    @Test
    void hashPassword() throws NoSuchAlgorithmException {
        String password = "test";
        String hashed = Hasher.hashPassword(password);

        String passwordA="password";
        String passwordB="password";
        String passwordC="password2";

        String hashedA = Hasher.hashPassword(passwordA);
        String hashedB = Hasher.hashPassword(passwordB);
        String hashedC = Hasher.hashPassword(passwordC);
        assertNotEquals(null, hashed);
        assertEquals(hashedA,hashedB);
        assertNotEquals(hashedA,hashedC);


    }

    @Test
    void checkPassword() throws NoSuchAlgorithmException {
        String password = "test";
        String hashed = Hasher.hashPassword(password);
        assertTrue(Hasher.checkPassword(password, hashed));
        password="";
        hashed=Hasher.hashPassword(password);
        assertTrue(Hasher.checkPassword(password, hashed));
    }

}