package be.ac.ulb.infof307.g10.model;

import be.ac.ulb.infof307.g10.model.DAO.XMLParserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class TestXMLParserDAO {
        private XMLParserDAO parser;
        private ArrayList<Article> list;

    @BeforeEach
    void init(){
        parser = new XMLParserDAO();
        list = new ArrayList<>();
    }

    @Test
    void parseArticles() {
        assertNotEquals(list,parser.parseArticles("https://www.dhnet.be/rss/section/actu.xml"));
    }


}