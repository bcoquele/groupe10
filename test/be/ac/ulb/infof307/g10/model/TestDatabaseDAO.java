package be.ac.ulb.infof307.g10.model;

import be.ac.ulb.infof307.g10.model.DAO.DatabaseDAO;
import de.saxsys.javafx.test.JfxRunner;
import de.saxsys.javafx.test.TestInJfxThread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(JfxRunner.class)
class TestDatabaseDAO {

    private DatabaseDAO db;
    private Hasher hash;

    @BeforeEach
    void init() {
        db = DatabaseDAO.getInstance();
        db.open();
    }

    /**
     * Test if the user is added to the database.
     * @throws NoSuchAlgorithmException
     */
    @Test
    void addUser() throws NoSuchAlgorithmException {
        try {
            db.addUser("tester",Hasher.hashPassword("tester"));
        } catch (SQLException e) {}
        assertTrue(db.checkUser("tester",Hasher.hashPassword("tester")));
    }

    /**
     * Test if the database is opened.
     * @throws SQLException
     */
    @Test
    void open() throws SQLException {
        DatabaseDAO db2 = DatabaseDAO.getInstance();
        db2.open();
        assertFalse(db2.getConnection().isClosed());
    }

    /**
     * Test if the database is closed.
     * @throws SQLException
     */
    @Test
    void close() throws SQLException {
        db.close();
        assertTrue(db.getConnection().isClosed());
    }


    @Test
    @TestInJfxThread
    void checkUser() {
        try {
            db.addUser("Jean", "Luc");
        } catch (SQLException e) {
        }
        assertTrue(db.checkUser("Jean","Luc"));
        assertFalse(db.checkUser("Jean","Matthieu"));
        assertFalse(db.checkUser("Test","Luc"));
    }

}