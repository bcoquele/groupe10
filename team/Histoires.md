# Histoires
Informations récapitulatives concernant les différentes histoires.

#### Quelques précisions
Un point correspond à une heure de travail par binôme (approximatif).  Par itération il faut accomplir X points.

----------------------


## Pondération

| Priorité/3 | N° | Description | Difficulté/3 | Risque/3 | Heures/? | Points |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 1 | 1 | Visualisation des articles | 5 | 3 | 50 | 35 |
|   | 3 | Récupération des articles à partir des sources extérieures | 5 | 2 | 35 | 25 |
|   |[4](#Histoire-A) | Récupération des articles à partir de pages web | 7 | 1 | 49 | 20 |
| 2 | 2 | Recherche et filtres sur les articles stockés localement | 5 | 2 | 40 | 45 |
|   | 5 | Création d’un système de recommandation | 5 | 2 | 35 | à définir (min 20) |
|   | 7 | Intégrité des données | 6 | 1 | 42 | 35 |
|   | 9 | Vérification de la fiabilité d’un article | 6 | 1 | 42 | 40 |
|   | 10 | Gestion des articles | 6 | 3 | 42 | 45 |
|   | 11 | Gestion de plusieurs utilisateurs | 6 | 2 | 42 | 25 |
|   | 12 | Support pour des médias différents (vidéo, images, etc.) | 6 | 1 | 42 | 25 |
|   | 14 | Filtrage des articles sur base de la rélevance géographique (sélection d’une région à l’aide d’une carte) | 6 | 1 | 42 | à définir |
| 3 | 6 | Sécurité des données | 6 | 1 | 42 | 55 |
|   | 8 | Intégration avec des réseaux sociaux | 6 | 1 | 42 | à définir (min 37) |
|   | 13 | Continuous learning pour le système de recommandation | 6 | 3 | 42 | 25 |
|   | 15| Section d’aide | 6 | 3 | 42 | 5-20 |

----------------------


## Description

### Histoire 4

**Instructions originales:**           
- Rien qui ne dévie de l'histoire originale.

**Tâches en plus:**          
- Rien

**Question:**       
- Que se passe-t-il lorsque l'utilisatur rentre une adresse Web qui ne contient pas d'articles ? ou bien contient une liste d'articles ?
    - Le software renvoie un message d'erreur ou bien une liste parmi laquelle l'utilisateur choisit un article.


