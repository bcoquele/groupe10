package be.ac.ulb.infof307.g10.view;

import be.ac.ulb.infof307.g10.controller.LoginMenu;
import be.ac.ulb.infof307.g10.model.DAO.DatabaseDAO;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Controller of Login
 */
public class LoginController {

    private LoginMenu controller;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private Button login;

    @FXML
    private Button register;

    private DatabaseDAO db;

    /**
     * Action when button "Se Connecter" is pressed
     * Recuperation of the username and password
     * and try to login with those informations
     */
    @FXML
    private void handleLoginAction() throws NoSuchAlgorithmException {
        String givenUsername = username.getText();
        String givenPassword = password.getText();

        controller.login(givenUsername,givenPassword);

    }
    /**
     * Action when button "Créer un compte" is pressed
     * Recuperation of the username and password
     * and try to register with those informations
     */
    @FXML
    private void handleRegisterAction() throws NoSuchAlgorithmException, SQLException {
        String givenUsername = username.getText();
        String givenPassword = password.getText();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Conditions d'utilisation");
        alert.setHeaderText("Approuvez-vous nos conditions ?");
        alert.setContentText("Il faut être gentil. \nIl ne faut pas être méchant.");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            controller.register(givenUsername,givenPassword);
        } else {
            alert.close();
        }
    }
    /**
     * sets the controller
     * @param controller controller object from class LoginMenu
     */
    public void setLoginMenu(LoginMenu controller) {
        this.controller = controller;
    }
}
