package be.ac.ulb.infof307.g10.view;

import be.ac.ulb.infof307.g10.util.DialogBuilder;
import be.ac.ulb.infof307.g10.controller.MainApp;
import be.ac.ulb.infof307.g10.model.Article;
import be.ac.ulb.infof307.g10.model.DAO.DatabaseDAO;
import be.ac.ulb.infof307.g10.model.DAO.HTMLParserDAO;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Controller of ContentWindow
 */
public class ContentWindowController {
    @FXML
    private TextArea title;
    @FXML
    private TextArea content;
    @FXML
    private ImageView thumbnail;
    @FXML
    private Button delete;

    private final Article article;
    private MainApp mainApp;

    private javafx.scene.Node thisNode;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     * @param article an object article from class Article
     */
    public ContentWindowController(Article article){
        this.article = article;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        //String dataContent = HTMLParserDAO.contentParsing("https://www.lesoir.be/212247/article/2019-03-14/le-cardinal-godfried-danneels-est-decede");
        String dataContent;
        try {
            dataContent = HTMLParserDAO.contentParsing(article.getSource());
        } catch (IOException e) {
            dataContent = "Le contenu ne peut pas être recupéré";
        }
        article.setContent(dataContent);

        title.setText(article.getTitle());
        Image image;
        if (article.getThumbnail() == null){
            image = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Buceph" +
                    "ala-albeola-010.jpg/800px-Bucephala-albeola-010.jpg");
        }else{
            image = new Image(article.getThumbnail());
        }
        thumbnail.setImage(image);
        content.setText(article.getContent());
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * @param mainApp reference to mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Action when button "Supprimer" is pressed
     * Delete the selected article from the MainWindow VBOX by using the javafx.scene.Node reference.
     */
    @FXML
    private void handleButtonAction(){

        // Deletes the article from de databse
        try {
            DatabaseDAO.getInstance().open();
            DatabaseDAO.getInstance().deleteArticle(article, mainApp.getCurrentUser());
            mainApp.deleteArticleFromMainWindow(thisNode);
        } catch (SQLException e) {
            DialogBuilder.exceptionDialog(e, "Couldn't delete the article.");
        }
        ((Stage) delete.getScene().getWindow()).close();
    }

    /**
     * Sets the javafx.Scene.Node that will be used as a reference to delete this article from the MainWindow VBOX
     * @param node is the node related to the article we are showing here
     */
    public void setNode(javafx.scene.Node node) {
        thisNode = node;
    }
}
