package be.ac.ulb.infof307.g10.view;

import be.ac.ulb.infof307.g10.controller.MainApp;
import be.ac.ulb.infof307.g10.model.Article;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.scene.Node;

/**
 * Controller of the ArticleModel
 */
public class ArticleModelController {
    @FXML
    private Button contentWindow;
    @FXML
    private Text title;
    @FXML
    private Text content;
    @FXML
    private Text localisation;
    @FXML
    private Text source;
    @FXML
    private Text date;
    @FXML
    private ImageView thumbnail;

    private final Article article;
    private MainApp mainApp;

    // Aricle models controllers are what fills our VBOX in MainWindow, we can use this reference
    // Do delete this from the VBOX
    private Node thisNode;

    public void setNode(Node node) {
        thisNode = node;
    }

    /**
     * The second constructor.
     * The constructor is called before the initialize() method.
     * @param article an object article from class Article
     */
    public ArticleModelController(Article article){
        this.article = article;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        //Solution temporaire : si l'URL est null (pas d'image reçu), ça fait planter le tout
        Image image;
        if (article.getThumbnail() == null){
            image = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Buceph" +
                    "ala-albeola-010.jpg/800px-Bucephala-albeola-010.jpg");
        } else{
            image = new Image(article.getThumbnail());
        }
        thumbnail.setImage(image);
        title.setText(article.getTitle());
        content.setText(article.getContent());
        localisation.setText(article.getLocalisation());
        source.setText(article.getSource());
        date.setText(article.getDate());
    }

    /**
     * Action when button "Afficher" is pressed
     * Display the selected article.
     */
    @FXML
    private void handleButtonAction(){
        mainApp.showContentWindow(article, thisNode);
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * @param mainApp reference to mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
}
