package be.ac.ulb.infof307.g10.view;

import be.ac.ulb.infof307.g10.controller.OverviewMenu;
import be.ac.ulb.infof307.g10.model.Article;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.sql.SQLException;

/**
 * Controller of Overview
 */
public class OverviewController {

    private OverviewMenu controller;

    @FXML
    private TableView<Article> articleTable;
    @FXML
    private TableColumn<Article, String> titleColumn;
    @FXML
    private TableColumn<Article, String> authorColumn;
    @FXML
    private TableColumn<Article, String> sourceColumn;
    @FXML
    private TableColumn<Article, TextField> directoryColumn;
    @FXML
    private Button confirmation;
    @FXML
    private CheckBox Thumbnail;
    @FXML
    private CheckBox Localisation;
    @FXML
    private CheckBox Keywords;
    @FXML
    private CheckBox Date;

    /**
     * The data as an observable list of Articles.
     */
    private final ObservableList<Article> urlArticleList;


    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     * @param urlArticleList an observable list of Articles.
     */
    public OverviewController(ObservableList<Article> urlArticleList) {

        this.urlArticleList = urlArticleList;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     *
     */
    @FXML
    private void initialize() {
        // Add observable list data to the table
        articleTable.setItems(urlArticleList);
        articleTable.setEditable(true);
        // Initialize the article table with the four columns.
        titleColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTitle()));
        authorColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAuthor()));
        sourceColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSource()));
        directoryColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getDirectory()));
    }



    /**
     * Action when button "Confirmer" is pressed.
     * Import all the articles in the Main Window.
     */
    @FXML
    private void handleButtonAction() {
        controller.articleToMainWindow(urlArticleList);
        urlArticleList.clear();
    }


    public void setOverviewMenu(OverviewMenu controller){this.controller = controller;}
}