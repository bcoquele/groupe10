package be.ac.ulb.infof307.g10.view;

import be.ac.ulb.infof307.g10.controller.AddMenu;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * Controller of MenuAdd
 */
public class MenuAddController {
    @FXML
    private Button search;
    @FXML
    private TextField addSiteAddress;

    private AddMenu controller;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public MenuAddController(){

    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        //Quick access to a known working site
        addSiteAddress.setText("https://www.dhnet.be/");
    }

    /**
     * Action when button "Chercher" is pressed
     */
    @FXML
    private void handleButtonAction(){
        controller.addArticle(addSiteAddress.getText());
    }

    /**
     * sets the controller
     * @param controller controller object from class AddMenu
     */
    public void setAddMenu(AddMenu controller){this.controller= controller;}

}
