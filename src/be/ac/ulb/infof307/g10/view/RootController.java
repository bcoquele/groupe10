package be.ac.ulb.infof307.g10.view;

import be.ac.ulb.infof307.g10.controller.RootMenu;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;

/**
 * Controller of Root
 */
public class RootController {
    @FXML
    private MenuItem add;

    private RootMenu controller;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public RootController(){

    }

    /**
     * Action when button "Ajouter" is pressed
     * Display the research window in which you put the url.
     */
    @FXML
    private void handleAddAction(){
        controller.openAddMenu();
    }

    /**
     * Set controller of root
     * @param controller reference to the controller
     */
    public void setRootMenu(RootMenu controller) {
        this.controller = controller;
    }

    /**
     * Action when button "Déconnexion" is pressed
     * Send back the user to the login window.
     */
    @FXML
    private void handleSignOutAction(){
        controller.signOut();
    }

}
