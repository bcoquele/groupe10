package be.ac.ulb.infof307.g10.view;

import be.ac.ulb.infof307.g10.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeView;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * Controller of MainWindow
 */
public class MainWindowController {

    private static final String ROOT = "Root";
    @FXML
    private VBox VBoxArticles;
    @FXML
    private TreeView fileTree;

    private ObservableList<Node> files = FXCollections.observableArrayList();
    private Repository root;


    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public MainWindowController(){

    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        root = new Repository(ROOT);
        root.setExpanded(true);
        List<Article> articles;
        fileTree.setRoot(root);
    }

    /**
     * Search for a repository by its name in a node's children
     * @param parent node where we're looking for the repository
     * @param name of the repository we are looking for
     * @return the repository if it exists, null if not
     */
    private Repository getRepository(Repository parent, String name){
        int size = parent.getChildren().size();
        Repository child;
        String childName;
        for (int i = 0 ; i < size ; i++){
            child = (Repository) parent.getChildren().get(i);
            childName = (String) child.getValue();
            if (childName.equals(name)){
                return child;
            }
        }
        return null;
    }

    public VBox getVBoxArticles() {
        return VBoxArticles;
    }

    public Repository getRoot(){
        return root;
    }

    public void resetRoot() {
        root = new Repository("Root");
        fileTree.setRoot(root);
    }

    /**
     * Add a repository in a node's children if it doesn't exist
     * @param name of the repository we want to create
     * @param parent node where we want to create a repository
     * @return the created or existing repository
     */
    public Repository addRepository(String name, Repository parent){
        Repository repository = getRepository(parent,name);
        if ( repository == null){
            Repository newRepository = new Repository(name);
            parent.addChild(newRepository);
            return newRepository;
        }
        return repository;
    }

}
