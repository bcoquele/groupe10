package be.ac.ulb.infof307.g10.util;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Contains every alert message
 */
public class DialogBuilder {

    /**
     * Open an alert if there is any issue with the website url.
     * @param url URL of website
     */

    public static void invalidHostDialog(String url) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Adresse invalide");
        alert.setHeaderText("Impossible de trouver: "+url);
        alert.setContentText("Veuillez choisir un url correct !");
        alert.showAndWait();
    }

    /**
     * Show the stackTrace of an util.
     * @param e exception thrown
     */

    public static void exceptionDialog(Exception e, String string) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Message d'erreur");
        alert.setHeaderText("");
        alert.setContentText(string);

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("Erreur:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    /**
     * Open an alert if there is any issue with the database.
     */
    public static void databaseAccessErrorDialog(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ERREUR");
        alert.setHeaderText(null);
        alert.setContentText("Impossible de créer ou d'accéder à la base de données.");
        alert.showAndWait();
    }

    /**
     * Shows an alert
     */
    public static void alertMessage(String errorMessage){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ERREUR");
        alert.setHeaderText(null);
        alert.setContentText(errorMessage);

        alert.showAndWait();
    }

}
