package be.ac.ulb.infof307.g10.util;

import java.sql.SQLException;

/**
 * Custom exception for sql duplicate
 */
public class DuplicateKeyException extends SQLException{
    // Constructor that accepts a message
    public DuplicateKeyException()
    {
        super("Key already present in the list.");
    }
}
