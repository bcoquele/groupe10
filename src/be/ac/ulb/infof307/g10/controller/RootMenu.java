package be.ac.ulb.infof307.g10.controller;

/**
 * RootMenu links scrollbar menu from RootController to the MainWindow
 */
public class RootMenu {

    private final MainApp mainApp;

    /**
     * Controller of Root
     * @param mainApp reference to mainApp
     */

    public RootMenu(MainApp mainApp){
        this.mainApp=mainApp;
    }

    /**
     * Open AddMenu when button "Ajouter" is clicked
     */

    public void openAddMenu(){
        mainApp.showMenuAdd();
    }

    /**
     * Sign out the user by closing the mainWindow and setting the current user to null
     */
    public void signOut(){
        mainApp.closePrimaryStage();
        mainApp.setCurrentUser(null);
        mainApp.showLoginPage();
    }
}
