package be.ac.ulb.infof307.g10.controller;

import be.ac.ulb.infof307.g10.model.Article;
import be.ac.ulb.infof307.g10.model.DAO.DatabaseDAO;
import be.ac.ulb.infof307.g10.model.DAO.HTMLParserDAO;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.sql.SQLException;

/**
 * OverviewMenu links the button from OverviewController to the database
 */
public class OverviewMenu {

    private final MainApp mainApp;

    public OverviewMenu(MainApp mainApp){
        this.mainApp = mainApp;
    }

    /**
     * Give article list to MainWindow to show
     * @param articleList the list of the articles that the window has to show
     */

    public void articleToMainWindow(ObservableList<Article> articleList){

        for(Article article : articleList){
            getContentForArticle(article);
        }

        DatabaseDAO db = DatabaseDAO.getInstance();
        db.open();
            for (Article art : articleList){
                try {
                    db.insertArticle(mainApp.getCurrentUser(),art);
                } catch (SQLException e) {
                }
            }
        mainApp.closeWindow();
        try {
            mainApp.addArticleFromDB();
        } catch (SQLException e) {
        }

    }

    /**
     * The content of the article is set to the
     * object article from class Article. Before the call
     * to this method, the content was null
     * @param article article from class Article
     */
    private void getContentForArticle(Article article){
        String dataContent;
        try {
            dataContent = HTMLParserDAO.contentParsing(article.getSource());
        } catch (IOException e) {
            dataContent = "Le contenu ne peut pas être recupéré";
        }
        article.setContent(dataContent);
    }
}
