package be.ac.ulb.infof307.g10.controller;

import be.ac.ulb.infof307.g10.model.Article;
import be.ac.ulb.infof307.g10.model.DAO.HTMLParserDAO;
import be.ac.ulb.infof307.g10.model.DAO.XMLParserDAO;
import be.ac.ulb.infof307.g10.util.DialogBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.rmi.UnknownHostException;
import java.util.ArrayList;

/**
 * AddMenu links the button from MenuAddController to parse the HTML given and open OVERVIEW
 */
public class AddMenu {
    private final MainApp mainApp;
    private final ObservableList<Article> urlArticleList = FXCollections.observableArrayList();

    public AddMenu(MainApp mainApp){
        this.mainApp = mainApp;
    }

    /**
     * Get url and parse to give article list to overview
     * @param url website address from which we fetch xml
     */

    public void addArticle(String url){
        mainApp.closeWindow();
        HTMLParserDAO xmlSearch = new HTMLParserDAO();
        XMLParserDAO parser = new XMLParserDAO();
        ArrayList<String> rssLinks;

        try {
            xmlSearch.rssListFinder(url);
            rssLinks = xmlSearch.getXmlLinkList();
        } catch (UnknownHostException e) {
            be.ac.ulb.infof307.g10.util.DialogBuilder.invalidHostDialog(url);
            rssLinks = new ArrayList<>();
        } catch (IOException e) {
            DialogBuilder.exceptionDialog(e, "Impossible de trouver les articles sur " + url);
            rssLinks = new ArrayList<>();
        }

        int count = 0;
        for (String str : rssLinks) {
            if (count > 10) {
                break;
            }
            ArrayList<Article> articles = parser.parseArticles(str);
            if (articles != null) {
                for (Article art : articles) {
                    if (count < 10) {
                        urlArticleList.add(art);
                        count++;
                    } else {
                        break;
                    }
                }
            }
        }
        mainApp.showOverview(urlArticleList);
    }
}