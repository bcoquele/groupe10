package be.ac.ulb.infof307.g10.controller;

import be.ac.ulb.infof307.g10.model.DAO.DatabaseDAO;
import be.ac.ulb.infof307.g10.util.DialogBuilder;
import be.ac.ulb.infof307.g10.view.*;
import be.ac.ulb.infof307.g10.model.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Main application, initializes the windows and manages them
 */
public class MainApp extends Application {
    private Stage primaryStage;
    private BorderPane rootLayout;
    private MainWindowController mainWindowController;
    private final LoginMenu loginMenu;
    private final RootMenu rootMenu;
    private final AddMenu addMenu;
    private final OverviewMenu overviewMenu;
    private FXMLLoader loader;
    private Stage dialogStage;
    private String currentUser = null;

    /**
     * The data as an observable list of Articles.
     */
    private final ObservableList<Article> urlArticleList = FXCollections.observableArrayList();




    /**
     * Constructor
     */
    public MainApp() {
        loginMenu = new LoginMenu(this);
        rootMenu = new RootMenu(this);
        addMenu = new AddMenu(this);
        overviewMenu = new OverviewMenu(this);
    }

    /**
     * Initializes the main window
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("FeedBuzz");

        showLoginPage();
    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/Root.fxml"));
            rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

            RootController controller = loader.getController();
            controller.setRootMenu(rootMenu);

        } catch (IOException e) {
        }
    }

    /**
     * Show the main window with all the articles the user added
     */
    public void showMainWindow(){
        try {
            loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/MainWindow.fxml"));
            BorderPane mainWindow = loader.load();

            rootLayout.setCenter(mainWindow);

            mainWindowController = loader.getController();
            addArticleFromDB();

        } catch (IOException | SQLException e) {
        }
    }

    /**
     * Opens a dialog to edit details for the specified URL from where the articles are imported.
     *
     */
    public void showMenuAdd() {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/MenuAdd.fxml"));
            AnchorPane page = loader.load();

            MenuAddController controller = loader.getController();
            controller.setAddMenu(addMenu);

            stageCreation("import URL", page);


        } catch (IOException e) {
        }
    }

    /**
     * Opens a dialog where the user can login or register
     */
    public void showLoginPage() {
        try {
            // Load the fxml file and create a new stage.
            loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/Login.fxml"));
            AnchorPane page = loader.load();

            LoginController controller = loader.getController();
            controller.setLoginMenu(loginMenu);

            stageCreation("Connexion", page);


        } catch (IOException e) {
        }
    }

    /**
     * Opens a dialog to edit details to confirm the addition of the articles.
     */
    public void showOverview(ObservableList<Article> urlArticleList) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            OverviewController controller = new OverviewController(urlArticleList);
            controller.setOverviewMenu(overviewMenu);
            loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("../view/Overview.fxml"));
            loader.setController(controller);
            AnchorPane page = loader.load();


            stageCreation("Ajout d'articles", page);

        } catch (IOException e) {
        }
    }

    /**
     * Opens a dialog to edit details for the specified URL from where the articles are imported.
     *
     */
    public void showContentWindow(Article article, javafx.scene.Node node) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            loader = new FXMLLoader();
            ContentWindowController controller = new ContentWindowController(article);
            controller.setNode(node);
            loader.setLocation(MainApp.class.getResource("../view/ContentWindow.fxml"));
            loader.setController(controller);
            controller.setMainApp(this);
            AnchorPane page = loader.load();

            stageCreation("Contenu de l'article séléctionné",page);

        } catch (IOException e) {
        }
    }

    /**
     * Retrieve from the DB and show in the main window all the user's articles
     * @throws SQLException if query fails
     */
    public void addArticleFromDB() throws SQLException {
        mainWindowController.getVBoxArticles().getChildren().clear();
        DatabaseDAO db = DatabaseDAO.getInstance();
        db.open();
        addArticleInMainWindow(db.retrieveArticle(currentUser));
        db.close();
        mainWindowController.getRoot().getChildren().removeAll();
        showArticlesInTableView();
    }

    /**
     * Add the AnchorPane (article) into the VBox of the MainWindow.
     */
    private void addArticleInMainWindow(ArrayList<Article> articleArrayList) {
        for (Article article : articleArrayList) {
            try {
                //Load the fxml file and create a new stage for the popup dialog.
                loader = new FXMLLoader();
                ArticleModelController controller = new ArticleModelController(article);
                controller.setMainApp(this);
                loader.setLocation(MainApp.class.getResource("../view/ArticleModel.fxml"));
                loader.setController(controller);

                AnchorPane page = loader.load();

                page.getChildren();
                controller.setNode(page);
                mainWindowController.getVBoxArticles().getChildren().add(page);

            } catch (IOException e) {
            }
        }
    }

    /**
     * Deletes an article from the VBox with it's node and redraws the ThreeView
     * @param node article cell in vbox
     */
    public void deleteArticleFromMainWindow(javafx.scene.Node node) {
        mainWindowController.getVBoxArticles().getChildren().remove(node);
        try {
            showArticlesInTableView();
        } catch (Exception e) {
            DialogBuilder.exceptionDialog(e, "Impossible de supprimer l'article !");
        }
    }

    /**
     * Get the directories informations from the DatabaseDAO and add it in the tableView
     */
    private void showArticlesInTableView() throws SQLException {
        DatabaseDAO db = DatabaseDAO.getInstance();
        db.open();
        ArrayList<HashMap<String,String>> userArticles = db.getUserArticlesPath(currentUser);
        db.close();

        mainWindowController.resetRoot();

        String[] path;
        Repository currentRepository;
        for(HashMap<String,String> article : userArticles){
            if (article.get("directory").equals("") || article.get("directory") == null){
                mainWindowController.getRoot().addChild(new File(article.get("title")));
            }
            else{
                path = article.get("directory").split("/");      //Parse the path into repositories
                currentRepository = mainWindowController.getRoot();
                for (String repository : path){
                    currentRepository = mainWindowController.addRepository(repository, currentRepository);
                }
                currentRepository.addChild(new File(article.get("title")));
            }
        }
    }

    /**
     * Create the dialogue window and show its content
     * @param title title of the window
     * @param page content of the window
     */
    private void stageCreation(String title, AnchorPane page){
        dialogStage = new Stage();
        dialogStage.setTitle(title);
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(page);
        dialogStage.setScene(scene);

        // Show the dialog and wait until the user closes it
        dialogStage.show();
    }

    /**
     * Close the current stage.
     */
    public void closeWindow(){
        dialogStage.close();
    }

    /**
     * Close the primary stage.
     */
    public void closePrimaryStage(){
        primaryStage.close();
    }


    public static void main(String[] args) {
        launch(args);
    }

    /**
     *
     * @return a string with the name of the current user
     */
    public String getCurrentUser() {
        return currentUser;
    }

    /**
     * set the current User
     * @param currentUser user currently using the application
     */
    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }
}