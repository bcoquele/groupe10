package be.ac.ulb.infof307.g10.controller;

import be.ac.ulb.infof307.g10.model.DAO.DatabaseDAO;
import be.ac.ulb.infof307.g10.util.DialogBuilder;
import be.ac.ulb.infof307.g10.model.Hasher;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

/**
 * LoginMenu links the buttons of LoginController to the database and open MainWindow
 */
public class LoginMenu {

    private final MainApp mainApp;

    /**
     * Controller is used as connection between the login window and the database
     * @param mainApp main application
     */
    public LoginMenu(MainApp mainApp){
        this.mainApp = mainApp;
    }

    /**
     * Add a new user to the database with a hashed password done with Hasher class
     * @param username Username provided by the user in the login window
     * @param password Password provided by the user in the login window
     * @throws NoSuchAlgorithmException util
     */

    public void register(String username, String password) throws NoSuchAlgorithmException, SQLException {
        DatabaseDAO db = DatabaseDAO.getInstance();
        db.open();
        try {
            db.addUser(username, Hasher.hashPassword(password));

        } catch (SQLException e) {
            DialogBuilder.databaseAccessErrorDialog();
            throw e;
        }
    }

    /**
     * Check if the user is part of the database, if true open the mainWindow
     * @param username see register
     * @param password see register
     * @throws NoSuchAlgorithmException util
     */

    public void login(String username, String password) throws NoSuchAlgorithmException {
        DatabaseDAO db = DatabaseDAO.getInstance();
        db.open();
        if(db.checkUser(username, Hasher.hashPassword(password))) {
            mainApp.closeWindow();
            mainApp.initRootLayout();
            mainApp.setCurrentUser(username);
            mainApp.showMainWindow();
        }else{
            DialogBuilder.databaseAccessErrorDialog();
        }
        try {
            db.close();
        } catch (SQLException ignored) {}
    }
}


