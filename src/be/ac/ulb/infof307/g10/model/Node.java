package be.ac.ulb.infof307.g10.model;

import javafx.scene.control.TreeItem;

/**
 * Structure
 */
public abstract class Node extends TreeItem {

    private String name;

    /**
     * Constructor, use TreeItem's constructor
     * @param name of the node
     */
    Node(String name){
        super(name);
    }

    public abstract void addChild(Node child);

    /**
     * Get the name of the Node
     * @return the name as a String
     */
    public String getName() {
        return name;
    }
}
