package be.ac.ulb.infof307.g10.model;

/**
 * Structure
 */
public class Repository extends Node {

    /**
     * Constructor, use Node's constructor and set as expanded
     * @param name of the created Repository
     */
    public Repository(String name){
        super(name);
        this.setExpanded(true);
    }

    /**
     * Add a child to the Repository
     * @param child we want to add
     */
    public void addChild(Node child) {
        this.getChildren().add(child);
    }

}
