package be.ac.ulb.infof307.g10.model;

import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Used to temporary stock information of given article
 */
public class Article {
    private String title;
    private String content;
    private String author;
    private String source;
    private String thumbnail;
    private String localisation;
    private ArrayList<String> keyWords;
    private String date;
    private TextField directory;
    /**
     * Initialises the directories array as a list with a "" string into it
     * If the article is in the directory war/syria, then this list would
     * contain two strings: "war", "syria" in this respective order
     */
    private List<String> directories = new ArrayList<>(Arrays.asList(""));

    /**
     *
     * @param directories repertories list
     */
    public void setDirectories(List<String> directories) {

        this.directories = directories;
    }


    public Article(){}

    /**
     * Contains information about the retrieved articles
     * @param title title of the article
     * @param author author of the article
     * @param source  url link of the source of the article
     */
    private Article(String title, String author, String source){

        this.title =  title;
        this.author =  author;
        this.source =  source;
    }

    /**
     * Article is a simple information container. The informations are mainly stored as String or Date objects.
     * The only methods are getters and setters and the Article object is serializable
     */
    public Article(String title, String author, String source, String urlImage){
        this(title, author, source);
        thumbnail = urlImage;
        this.directory = new TextField();
    }

    /**
     *
     * @return the title of the article
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @return the content of the article
     */
    public String getContent() {
        return content;
    }

    /**
     *
     * @return the author of the article
     */
    public String getAuthor() {
        return author;
    }

   /**
     *
     * @param title title of the article
     */
    public void setTitle(String title){
        this.title =  (title);
    }

   /**
     *
     * @param author author of the article
     */
    public void setAuthor(String author) {
        this.author =  (author);
    }

    /**
     *
     * @param content content of the article
     */
    public void setContent(String content) {
        this.content =  (content);
    }

    /**
     *
     * @return the source of the article
     */
    public String getSource() {
        return source;
    }

    /**
     *
     * @return the url of the image article
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     *
     * @return the localisation of the event of the article
     */
    public String getLocalisation() {
        return localisation;
    }

    /**
     *
     * @return a list of keyWords of the article
     */
    public ArrayList<String> getKeyWords() {
        return keyWords;
    }

    /**
     *
     * @return the date of the article
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @return the directory where the article is placed
     */
    public TextField getDirectory(){return directory;}

    /**
     *
     * @return the directory content (format String)
     */
    public String getDirectoryContent(){return directory.getText();}

    /**
     *
     * @param source source of the article
     */
    public void setSource(String source) {
        this.source =  (source);
    }

    /**
     *
     * @param thumbnail the url of the image article
     */
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     *
     * @param localisation localisation of the event of the article
     */
    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    /**
     *
     * @param keyWords list of keyWords of the article
     */
    public void setKeyWords(ArrayList<String> keyWords) {
        this.keyWords = keyWords;
    }

    /**
     *
     * @param date date of the article
     */
    public void setDate(String date) {
        this.date = date;
    }
}
