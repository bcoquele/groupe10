package be.ac.ulb.infof307.g10.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Crypt password
 */
public abstract class Hasher {

    private static final String HASH_TYPE = "SHA-256";

    /**
     * Hashes the password in SHA-256
     * @param toHash the password to hash
     * @return the hashed password in SHA-256
     */
    public static String hashPassword(String toHash) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(HASH_TYPE);
        byte[] bytes = md.digest(toHash.getBytes());
        StringBuilder generatedPassword = new StringBuilder();
        for (byte aByte : bytes) {
            generatedPassword.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }
        return generatedPassword.toString();
    }

    /**
     * Rehashes the password to check and compares it with the previously hashed password (stored in database)
     * @param toCheck the password we need to check validity
     * @param hashedPassword the previously hashed password stored in the database
     * @return true if the password is valid
     */
    public static boolean checkPassword(String toCheck, String hashedPassword) throws NoSuchAlgorithmException {
        return (hashedPassword.equals(hashPassword(toCheck)));
    }
}
