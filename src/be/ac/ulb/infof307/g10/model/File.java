package be.ac.ulb.infof307.g10.model;

/**
 * Structure
 */
public class File extends Node {

    /**
     * Constuctor, use Node's constructor
     * @param name of the created File
     */
    public File(String name){
        super(name);
    }

    /**
     * Do nothing as a file can't have children
     * @param child child to add
     */
    public void addChild(Node child) {

    }
}


