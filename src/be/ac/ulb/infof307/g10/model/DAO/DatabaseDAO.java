package be.ac.ulb.infof307.g10.model.DAO;

import be.ac.ulb.infof307.g10.util.DialogBuilder;
import be.ac.ulb.infof307.g10.util.DuplicateKeyException;
import be.ac.ulb.infof307.g10.model.Article;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Data Access Object of the database.
 */
public class DatabaseDAO {
    private final String CONNECTION_URL = "jdbc:derby:" + "UserDatabase" + ";create=true";
    private Connection conn = null;
    private static final int DUPLICATE_KEY_CODE = 30000;

    private static DatabaseDAO instance = null;

    //Preparing request to create Users DatabaseDAO
    private static final String TABLE_GENERIC = "GENERIC";
    private static final String TABLE_GENERIC_COLUMN_USER = "username";
    private static final String TABLE_GENERIC_COLUMN_PASSWORD = "password";

    private static final String CREATE_TABLE_GENERIC = "CREATE TABLE " + TABLE_GENERIC + " ( " +
            TABLE_GENERIC_COLUMN_USER + " varchar(100), " +
            TABLE_GENERIC_COLUMN_PASSWORD + " varchar(100), " + "primary key (" +
            TABLE_GENERIC_COLUMN_USER + " ))";

    //Preparing request to create Articles Table
    private static final String TABLE_ARTICLE = "ARTICLE";
    private static final String TABLE_ARTICLE_TITLE = "title";
    private static final String TABLE_ARTICLE_USER = "articleUser";
    private static final String TABLE_ARTICLE_AUTHOR = "author";
    private static final String TABLE_ARTICLE_SOURCE = "source";
    private static final String TABLE_ARTICLE_THUMBNAIL = "thumbnail";
    private static final String TABLE_ARTICLE_CONTENT = "content";
    private static final String TABLE_ARTICLE_DIRECTORY = "directory";
    private static final String CREATE_TABLE_ARTICLE = "CREATE TABLE " + TABLE_ARTICLE + " ( " +
            TABLE_ARTICLE_TITLE + " varchar(200), " +
            TABLE_ARTICLE_USER + " varchar(100), " +
            TABLE_ARTICLE_AUTHOR + " varchar(100), " +
            TABLE_ARTICLE_SOURCE + " varchar(300), " +
            TABLE_ARTICLE_THUMBNAIL + " varchar(200), " +
            TABLE_ARTICLE_CONTENT + " long varchar, " +
            TABLE_ARTICLE_DIRECTORY + " varchar(200), " +
            "primary key (" + TABLE_ARTICLE_TITLE + ", " + TABLE_ARTICLE_USER + ") , " +
            "CONSTRAINT " + "user_fk " + "FOREIGN KEY (" + TABLE_ARTICLE_USER + ") " +
            "REFERENCES " + TABLE_GENERIC + " (" + TABLE_GENERIC_COLUMN_USER + "))";


    /***
     * Private constructor to prevent multiple instantiation
     */
    private DatabaseDAO() {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
            this.conn = DriverManager.getConnection(CONNECTION_URL);
            createDBTable(TABLE_GENERIC, CREATE_TABLE_GENERIC);
            createDBTable(TABLE_ARTICLE, CREATE_TABLE_ARTICLE);
        } catch (Throwable e) {
            DialogBuilder.databaseAccessErrorDialog();
        }
    }

    /**
     * Open a connection to Derby
     */
    public void open() {
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
        } catch (SQLException e) {
            DialogBuilder.databaseAccessErrorDialog();
        }
    }

    /***
     * Close the connection to Derby
     *
     */
    public void close() throws SQLException {
        if (this.conn != null && !this.conn.isClosed())
            this.conn.close();
    }

    /***
     * Return the only instance of the DAO (it's a singleton).
     * The synchronized is mandatory because of the multi-threads coming from JavaFX.
     * @return instance of DAO
     */
    public synchronized static DatabaseDAO getInstance() {
        if (instance == null) {
            instance = new DatabaseDAO();
        }
        return instance;
    }

    /***
     * return the DAO, a connection object from jdbc
     * @return connection
     */
    public Connection getConnection() {
        return this.conn;
    }

    /**
     * Check the existence of a database table.
     * If the database table doesn't exist, it creates a new one.
     *
     * @param tableName the name of the data based to be searched
     * @param createTableQuery SQL statement to create the database table
     */
    private void createDBTable(String tableName, String createTableQuery) {
        //try-with-resources statement
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL);
             PreparedStatement preparedStatement = conn.prepareStatement(createTableQuery)) { //Inutile si qu'une seule table dans la DB, à enlever si pas d'autre table (article parexemple)
            DatabaseMetaData meta = conn.getMetaData();
            ResultSet res = meta.getTables(null, null, null, null);

            boolean tableExists = false;

            while (res.next()) {
                if (res.getString("TABLE_NAME").contains(tableName)) {
                    tableExists = true;
                }
            }
            if (!tableExists) {
                preparedStatement.execute();
            }
        } catch (SQLException e) {
            DialogBuilder.databaseAccessErrorDialog();
        }
    }

    /***
     * Stores one Product in the database
     *
     * @param username user identifier
     * @param hashedPassword crypted password
     */
    public void addUser(String username, String hashedPassword) throws SQLException {
        try {
            PreparedStatement statement = this.conn.prepareStatement("insert into " + TABLE_GENERIC +
                    " (" + TABLE_GENERIC_COLUMN_USER + "," + TABLE_GENERIC_COLUMN_PASSWORD + ")values(?,?)");
            statement.setString(1, username);
            statement.setString(2, hashedPassword);
            statement.execute();
        } catch (SQLException exception) {
            while (exception != null) {
                if (exception.getErrorCode() == DUPLICATE_KEY_CODE) {
                    // The statement was aborted because it would have caused a duplicate key value
                    throw new DuplicateKeyException();
                } else {
                    DialogBuilder.databaseAccessErrorDialog();
                }
                exception = exception.getNextException();
            }
        }
    }

    /**
     * Check user password from database
     *
     * @param username username given by login
     * @param password password given by login
     * @return true if password match and false in other case
     */
    public boolean checkUser(String username, String password) {
        String querySQL = "SELECT * FROM " + TABLE_GENERIC +
                " WHERE " + TABLE_GENERIC_COLUMN_USER + " = ?";
        try (PreparedStatement statement = conn.prepareStatement(querySQL)) {
            statement.setString(1, username);
            ResultSet res = statement.executeQuery();
            res.next();                          //To get the password instead of the username
            if (res.getString(2).equals(password)) {
                res.close();
                return true;
            } else {
                res.close();
                return false;
            }
        } catch (SQLException e) {
            DialogBuilder.databaseAccessErrorDialog(); // Comment this line if you wanna test the util
        }
        return false;
    }

    /**
     * Insert an article associated to the current user in the database
     * @param username username of the current user
     * @param article an article from class Article
     * @throws SQLException if query fails
     */

    public void insertArticle(String username, Article article) throws SQLException {
        PreparedStatement statement = this.conn.prepareStatement("INSERT INTO " + TABLE_ARTICLE + " (" + TABLE_ARTICLE_USER +
                "," + TABLE_ARTICLE_TITLE + "," + TABLE_ARTICLE_AUTHOR + "," + TABLE_ARTICLE_CONTENT + "," + TABLE_ARTICLE_SOURCE + "," +
                TABLE_ARTICLE_THUMBNAIL +","+ TABLE_ARTICLE_DIRECTORY +")values(?,?,?,?,?,?,?)");
        statement.setString(1, username);
        statement.setString(2, article.getTitle());
        statement.setString(3, article.getAuthor());
        statement.setString(4, article.getContent());
        statement.setString(5, article.getSource());
        statement.setString(6, article.getThumbnail());
        statement.setString(7, article.getDirectoryContent());

        statement.execute();
    }

    /**
     * Get all the articles from the user in the DB
     * @param username from the user we want the articles
     * @return an ArrayList of articles
     * @throws SQLException if query fails
     */
    public ArrayList<Article> retrieveArticle(String username) throws SQLException {
        ArrayList<Article> articles = new ArrayList<>();
        PreparedStatement statement = this.conn.prepareStatement("SELECT * FROM " + TABLE_ARTICLE + " WHERE " +
                TABLE_ARTICLE_USER + " = ?");
        statement.setString(1, username);
        ResultSet res = statement.executeQuery();
        while (res.next()) {
            Article article = new Article(res.getString("TITLE"), res.getString("AUTHOR"), res.getString("SOURCE"), res.getString("THUMBNAIL"));
            // This might not work, maybe we will need a workaround
            String directoties_path = res.getString("DIRECTORY");
            String[] path_array = directoties_path.split("/");
            article.setDirectories(new ArrayList(Arrays.asList(path_array)));
            articles.add(article);
        }
        res.close();
        return articles;
    }

    /**
     * Get the titles and the paths of the user's articles in the DB
     * @param username of the user we want the articles
     * @return an ArrayList of Hashmap of(String, String) that have two keys : 'title' and 'directory'
     * @throws SQLException if query fails
     */
    public ArrayList<HashMap<String,String>> getUserArticlesPath(String username) throws SQLException {
        HashMap<String,String> article = new HashMap<>();
        ArrayList<HashMap<String,String>> articles = new ArrayList<>();
        PreparedStatement statement = this.conn.prepareStatement("SELECT TITLE, Article.DIRECTORY FROM " + TABLE_ARTICLE + " WHERE " +
                TABLE_ARTICLE_USER + " = ?");
        statement.setString(1, username);
        ResultSet res = statement.executeQuery();
        while(res.next()){
            article.put("title",res.getString("TITLE"));
            article.put("directory",res.getString("DIRECTORY"));
            articles.add(new HashMap<>(article));
        }
        res.close();
        statement.close();
        return articles;
    }

    /**
     * Deletes the article from the database.
     * @param article article to delete
     * @param currentUser user from which to delete the article
     * @throws SQLException if query fails
     */
    public void deleteArticle(Article article, String currentUser) throws SQLException {
        PreparedStatement statement = this.conn.prepareStatement("DELETE FROM " + TABLE_ARTICLE + " WHERE "+TABLE_ARTICLE_TITLE+" =? "+" AND "+TABLE_ARTICLE_USER+ " =? ");
        statement.setString(1, article.getTitle());
        statement.setString(2, currentUser);
        statement.execute();
    }
}


