package be.ac.ulb.infof307.g10.model.DAO;

import be.ac.ulb.infof307.g10.model.Article;
import be.ac.ulb.infof307.g10.util.DialogBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Parse the given XML link
 */
public class XMLParserDAO {

    private static final String AUTHOR_FORMAT1 = "author";
    private static final String AUTHOR_FORMAT2 = "dc:creator";
    private static final String TITLE = "title";
    private static final String LINK = "link";
    private static final String ENCLOSURE = "enclosure";
    private static final String URL_IMAGE = "url";
    private static final String DATE_FORMAT1 = "pubDate";
    private static final String DATE_FORMAT2 = "updated";
    private DocumentBuilder builder;

    /**
     * DOM Parser Initialisation
     */
    public XMLParserDAO() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException ignored) {
        }
    }

    /**
     * Access to the XML by URL from which article are taken and put into a list
     * @param url url of the xml file
     * @return list of article object after having parsed the xml
     */
    public ArrayList<Article> parseArticles(String url) {
        ArrayList<Article> articles = new ArrayList<>();
        try {
            Document doc = this.builder.parse(new URL(url).openStream());
            doc.getDocumentElement().normalize();
            NodeList items = doc.getElementsByTagName("item"); // This object contains all the articles we need
            if (items.item(0) == null) {                //Two ways to structure xml files, item and entry are both possible
                items = doc.getElementsByTagName("entry");
            }
            int itemCount = items.getLength();
            for (int i = 0; i < itemCount; i++) {
                articles.add(openArticle(items.item(i)));
            }
        } catch (Exception  e ) {
            DialogBuilder.alertMessage("Impossible de récuperer les articles.");
            return null;
        }
        return articles;
    }

    /**
     * Creation of a article object from the DOM parsing
     *
     * @param node references an article in the xml file
     * @return the filled article
     */
    private Article openArticle(Node node) {
        Element element = (Element) node;
        String author = getTagInfo(element, AUTHOR_FORMAT1);
        String date = getTagInfo(element, DATE_FORMAT1);
        if (author == null){
            author = getTagInfo(element, AUTHOR_FORMAT2);
        }
        if (date == null){
            date = getTagInfo(element, DATE_FORMAT2);
        }
        Article article = new Article(getTagInfo(element, TITLE), author,getTagInfo(element, LINK),getImageURL(element));
        article.setDate(date);
        return article;

    }

    /**
     *Extract image's info by tag
     *
     * @param element (the node casted as an Element)
     * @param tag references a xml tag
     * @return the info
     */
    private String getTagInfo(Element element, String tag){
        String info = null;
        if (element.getElementsByTagName(tag).item(0) != null) {
            info = element.getElementsByTagName(tag).item(0).getTextContent();
        }
        return info;
    }

    /**
     * Extract image's URL
     * @param element (the node casted as an Element)
     * @return the URL of the image as a String. If any, return null
     */
    private String getImageURL(Element element) {
        Element image = (Element) element.getElementsByTagName(ENCLOSURE).item(0);
        if ( image != null){
            return image.getAttribute(URL_IMAGE);

        }
        return  null;
    }
}
