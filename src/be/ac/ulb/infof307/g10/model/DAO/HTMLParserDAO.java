package be.ac.ulb.infof307.g10.model.DAO;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

/**
 * Parse the given HTML link
 */
public class HTMLParserDAO {

    private final ArrayList<String> xmlLinkList = new ArrayList<>();

    /**
     *
     * This function takes an url (String) and search for a link
     * to the RSS feeds page on the website. The page is saved in the rssLink variable.
     *
     */
    public void rssListFinder(String url) throws IOException {
        Document doc = Jsoup.connect(url).followRedirects(true).get();
        Elements links = doc.select("a[href],link[href]");
        for (Element link : links){

            if(link.attr("abs:href").toLowerCase().contains("/rss")){
                String rssLink = link.attr("abs:href");
                rssSpecificFinder(url, rssLink);
                break;
            }
        }
    }

    /**
     * This function gets the RSS feeds page from RssListFinder and gives the list
     * of all the xml files found on it. The list is xmlLinkList.
     */
    private void rssSpecificFinder(String domain, String rssLink) throws IOException {
        // In case we find the link of the RSS's XML file directy into the main website's page
        if (rssLink.contains(".xml") && !xmlLinkList.contains(rssLink)) {
            xmlLinkList.add(rssLink);
            return;
        }
        Document doc = Jsoup.connect(rssLink).followRedirects(true).get();
        Elements links = doc.select("a[href]");
        for (Element link : links){
            String temp_link = link.attr("abs:href".toLowerCase());
            if(temp_link.contains(".xml") && !xmlLinkList.contains(temp_link)
                    && ( checkDomain(temp_link, domain) || domainMatching(temp_link, domain))) {
                    xmlLinkList.add(link.attr("abs:href"));
            }
        }
    }

    /**
     * Checks the domain validity between the found url on the page and the initial url given by the user
     */
    private boolean checkDomain(String url, String rightDomain) {
        if (rightDomain==null || url==null) {
            return false;
        }
        try {
            URI uri = new URI(url);
            String urlDomain = uri.getHost();
            urlDomain = urlDomain.replace("www.","").replace("http://","")
                    .replace("https://","");
            rightDomain = rightDomain.replace("www.","").replace("http://", "")
                    .replace("https://","");

            // On retire le charactère '/' à la fin de l'URL si il existe.
            if (urlDomain.charAt(urlDomain.length()-1)=='/') urlDomain = urlDomain.substring(0, urlDomain.length()-1);
            if (rightDomain.charAt(rightDomain.length()-1)=='/') rightDomain = rightDomain.substring(0, rightDomain.length()-1);
            return urlDomain.equals(rightDomain);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * This is an alternative test, in verifies if there is a substring of the given url in the rightdomain
     * @param url The url we have to check the validity
     * @param rightDomain The domain we are searching in
     * @return true if it matches, false if not
     */
    private boolean domainMatching(String url, String rightDomain) {
        try {
            URI uri = new URI(url);
            String urlDomain = uri.getHost();
            rightDomain = rightDomain.replace("www.","").replace("http://","")
                    .replace("https://","");
            urlDomain = urlDomain.replace("www.","").replace("http://","")
                    .replace("https://","");
            if (urlDomain.charAt(urlDomain.length()-1)=='/') urlDomain = urlDomain.substring(0, urlDomain.length()-1);
            if (rightDomain.charAt(rightDomain.length()-1)=='/') rightDomain = rightDomain.substring(0, rightDomain.length()-1);
            return urlDomain.toLowerCase().contains(rightDomain.toLowerCase());
        } catch (Exception e) {
            return false;
        }
    }

    /**
     *
     * @return the ArraList of Xml Links
     */
    public ArrayList<String> getXmlLinkList() {
        return xmlLinkList;
    }


    /**
     * Gathers all the 'content' of a webpage, the content is everything that is included in "p" HTML tags
     * @param url the WebPage link
     * @return the content of the article
     */
    public static String contentParsing(String url) throws  IOException {
        Document doc = Jsoup.connect(url).get();

        Elements paragraphs = doc.getElementsByTag("p");
        String content = "";
        for (Element e : paragraphs) {
            content += e.ownText();
            content+= "\n\n";
        }
        return content;
    }
}
