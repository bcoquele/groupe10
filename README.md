# FeedBuzz : Projet de génie logiciel et gestion de projet (INFO-F-307)

Réaliser un agrégateur de flux RSS en Java. Celui-ci se sépare en histoires à compléter afin de réaliser les souhaits du client.

# Utilisation

Version Java : jdk1.8.0_201
Version IDE : IntelliJ 2018.2.7
Librairies : - JavaFX 8
			 - Jsoup 1.11.3
			 - JUnit 5

## Compilation

TO DO: Informations sur la façon de compiler votre projet 

## Démarrage 

TO DO: Informations sur la façon d'éxecuter votre projet

# Configuration :

## Serveur 

TO DO: Informations sur la configuration du serveur

## Client

TO DO: Informations sur la configuration du client

# Tests

TO DO: Informations sur la façon d'executer les tests

Fait par JUnit, pas d'ordre spécifique.

# Misc

## Développement

## Screenshot

## License
